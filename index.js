const express = require('express');
const app = express();
const port = 3000;
const manejaGet = (req, res) => {
    console.log("Estoy en la pagina home");
    res.json({saludo:'Hola mundo desde Node!'});
}
app.get('/home', manejaGet);

app.get('/omar', (req, res)=> {
    console.log('GET /omar');
    res.json({name: 'Omar Chigo Acua'});
});
app.listen(port, () => {
    console.log("El servidor esta escuchando en la url http://localhost:",port);
})